let Bicicleta = require("../../models/Bicicleta");



exports.bicicleta_list = function (req, res) {

    Bicicleta.allBicis(function (err, bicis) {
   
    if (err) res.status(500).send(err.message);
   
   
    res.status(200).json({
   
    bicicletas: bicis
   
    });
   
    });
   
   };
   exports.bicicleta_create = function (req, res) {

    let bici = new Bicicleta({
   
    bicicletaID: req.body.bicicletaID,
   
    color: req.body.color,
   
    modelo: req.body.modelo,
   
    ubicacion: req.body.ubicacion
   
    });
   
    Bicicleta.add(bici, function (err, newBici) {
   
    if (err) res.status(500).send(err.message);
   
    res.status(201).send(newBici);
   
    });
   
   };

   exports.bicicleta_delete = function(req,res){

    //Borrado con la funcion remove
    Bicicleta.removeById(req.body._id,(err,NoBici)=>{
        if(err)
    
          res.status(500).send(err.message);
          res.status(200).send(NoBici);
    
          
    
    });

};


exports.bicicleta_update = function(req,res){

    let bici = ({
  
      bicicletaID: req.body.bicicletaID,
      color: req.body.color,
      modelo: req.body.modelo,
      ubicacion: [req.body.latitud, req.body.longitud]
  
    })
      Bicicleta.updateById(bici,function(err,nbici){
      if (err) 
      res.status(500).send(err.message);
  
      res.status(200).send(nbici);
  
    });
  }
