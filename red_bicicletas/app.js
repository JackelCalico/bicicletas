var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var bicicletasRouter = require('./routes/bicicletas');

var usuariosRouter = require("./routes/usuarios");

var tokenRouter = require("./routes/tokens");

var app = express();
//Rutas de la Api
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');
var reservasAPIRouter = require("./routes/api/reservas");

//Mongoose

var mongoose = require('mongoose');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('/bicicletas', bicicletasRouter);

app.use("/token", tokenRouter);

app.use("/usuarios", usuariosRouter);

//Ruta de la api

app.use('/api/bicicletas', bicicletasAPIRouter); 
app.use('/api/usuarios',usuariosAPIRouter);
app.use("/api/reservas", reservasAPIRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//Conexion mongoose

mongoose.connect('mongodb://localhost/red_bicicletas', { useUnifiedTopology: true, useNewUrlParser: true  }); 

mongoose.Promise = global.Promise;

var db = mongoose.connection;
//Bind connection to error event (to get notification of connection errors)
db.on("error", console.error.bind('Error de conexión con MongoDB'));


module.exports = app;
